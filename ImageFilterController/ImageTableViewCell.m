
//
//  ImageTableViewCell.m
//  ImageFilterController
//
//  Created by parkyoseop on 2016. 9. 8..
//  Copyright © 2016년 Yoseop. All rights reserved.
//

#import "ImageTableViewCell.h"

@implementation ImageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.valueSlider setContinuous:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFilterType:(int)aFilterType
{
    self.valueSlider.value = 0.f;
    self.currentValueLabel.text = [NSString stringWithFormat:@"%@",@"기본값"];
    filterType = aFilterType;

    switch (aFilterType)
    {
        case GPUIMAGE_BRIGHTNESS:
        {
            _filterTitle = @"Brightness";
            
            filter = [[GPUImageBrightnessFilter alloc] init];
//            ((GPUImageBrightnessFilter *)filter).brightness = 0.1;
            self.valueSlider.value = 0.5f;
        }; break;
        case GPUIMAGE_SATURATION:
        {
            _filterTitle = @"Saturation";
            
            filter = [[GPUImageSaturationFilter alloc] init];
//            [(GPUImageSaturationFilter *)filter setSaturation:0.45];
            self.valueSlider.value = 0.5f;
        }; break;
        case GPUIMAGE_CONTRAST:
        {
            _filterTitle = @"Contrast";
            
            filter = [[GPUImageContrastFilter alloc] init];
//            ((GPUImageContrastFilter *)filter).contrast = 1.5;
            self.valueSlider.value = 0.25f;
        }; break;
        case GPUIMAGE_SHARPEN:
        {
            _filterTitle = @"Sharpen";
            
            filter = [[GPUImageSharpenFilter alloc] init];
//            [(GPUImageSharpenFilter *)filter setSharpness:0.6f];
//            self.currentValueLabel.text = [NSString stringWithFormat:@"%f",0.6f];
            self.valueSlider.value = 0.5f;
        }; break;
        case GPUIMAGE_VIGNETTE:
        {
            _filterTitle = @"Vignette";
            
            filter = [[GPUImageVignetteFilter alloc] init];
            self.valueSlider.value = 0.75f;
        }; break;
        
        case GPUIMAGE_GAUSSIAN:
        {
            _filterTitle = @"Gaussian Blur";

            filter = [[GPUImageGaussianBlurFilter alloc] init];
            self.valueSlider.value = 0.25f;
        }; break;
            
        default: filter = [[GPUImageSepiaFilter alloc] init]; break;
    }
    
    // ================================================================================
    // ================================================================================
    // ================================================================================
    // ================================================================================
    // ================================================================================
    // ================================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSLog(@"--------------- %@",_filterTitle);
        UIImage *inputImage = [UIImage imageNamed:@"testImage"];
        stillImageSource = [[GPUImagePicture alloc] initWithImage:inputImage];
        [stillImageSource addTarget:filter];
        [filter useNextFrameForImageCapture];
        [stillImageSource processImage];
        
        UIImage *filteredImage = [filter imageFromCurrentFramebuffer];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imageeeeView.image = filteredImage;
            self.imageeeeView.clipsToBounds = YES;
            self.rowLabel.text = [@(aFilterType) stringValue];
            self.filterNameLabel.text = _filterTitle;
        });
    });
}

- (IBAction)updateFilterFromSlider:(id)sender;
{
    if (self.animationIndicator.isAnimating) {
        return;
    }
    float value = [(UISlider *)sender value];
    float min;
    float max;
    switch(filterType)
    {
        case GPUIMAGE_BRIGHTNESS: {
            min = -0.8f;
            max = 0.8f;
            value = min + (value * (max-min));
            [(GPUImageBrightnessFilter *)filter setBrightness:value]; break;
        }
        case GPUIMAGE_SATURATION: {
            min = 0.5f;
            max = 1.5f;
            value = min + (value * (max-min));
            [(GPUImageSaturationFilter *)filter setSaturation:value]; break;
        }
        case GPUIMAGE_CONTRAST: {
            min = 0.5f;
            max = 3.5f;
            value = min + (value * (max-min));
            [(GPUImageContrastFilter *)filter setContrast:value]; break;
        }
        case GPUIMAGE_SHARPEN: {
            min = -3.5f;
            max = 3.5f;
            value = min + (value * (max-min));
            [(GPUImageSharpenFilter *)filter setSharpness:value]; break;
        }
        case GPUIMAGE_VIGNETTE: {
            min = 0.5f;
            max = 1.f;
            value = min + (value * (max-min));
            [(GPUImageVignetteFilter *)filter setVignetteEnd:value]; break;
        }
        case GPUIMAGE_GAUSSIAN: {
            min = 1.f;
            max = 7.f;
            value = min + (value * (max-min));
            [(GPUImageGaussianBlurFilter *)filter setBlurRadiusInPixels:value]; break;
        }
        default: break;
    }
    
    NSLog(@"=================%f", value);
    self.currentValueLabel.text = [NSString stringWithFormat:@"%.2f", value];
    [self.animationIndicator startAnimating];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSLog(@"--------------- %@",_filterTitle);
        UIImage *inputImage = [UIImage imageNamed:@"testImage"];
        stillImageSource = [[GPUImagePicture alloc] initWithImage:inputImage];
        [stillImageSource addTarget:filter];
        [stillImageSource processImage];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [filter useNextFrameForImageCapture];
            UIImage *filteredImage = [filter imageFromCurrentFramebuffer];
            if (filteredImage) {
                self.imageeeeView.image = filteredImage;
            }
            [self.animationIndicator stopAnimating];
        });
    });
}



@end
