//
//  AppDelegate.h
//  ImageFilterController
//
//  Created by parkyoseop on 2016. 9. 8..
//  Copyright © 2016년 Yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

