//
//  ImageTableViewCell.h
//  ImageFilterController
//
//  Created by parkyoseop on 2016. 9. 8..
//  Copyright © 2016년 Yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"

typedef enum {
    GPUIMAGE_BRIGHTNESS,
    GPUIMAGE_SATURATION,
    GPUIMAGE_CONTRAST,
    GPUIMAGE_SHARPEN,
    GPUIMAGE_VIGNETTE,
    GPUIMAGE_GAUSSIAN,
} GPUImageShowcaseFilterType;


@interface ImageTableViewCell : UITableViewCell
{
    GPUImageShowcaseFilterType filterType;
    GPUImageOutput<GPUImageInput> *filter;
    GPUImagePicture *sourcePicture;
    GPUImageUIElement *uiElementInput;
    GPUImageFilterPipeline *pipeline;
    GPUImagePicture *stillImageSource;
    
    NSString *_filterTitle;
}
@property (weak, nonatomic) IBOutlet UILabel *currentValueLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageeeeView;
@property (weak, nonatomic) IBOutlet UILabel *rowLabel;
@property (weak, nonatomic) IBOutlet UILabel *filterNameLabel;
@property (weak, nonatomic) IBOutlet UISlider *valueSlider;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *animationIndicator;

- (void)setFilterType:(int)aFilterType;


@end
